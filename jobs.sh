#!/bin/bash
rm -f jobs.yml
not_el7_packages=(nodejs-verdaccio
rubygem-colorize
rubygem-corineus
rubygem-open4
rubygem-Platform
rubygem-POpen4
rubygem-r10k_gitlab_webhook
rubygem-thor)
el8_packages=(bower
consul
corineus
golangci-lint
gradle
harbottle-main-release
kops
kubeadm1.16
kubeadm1.17
kubectl1.16
kubectl1.17
mattermost
mmctl
nodejs-bower
nodejs-verdaccio
packer
r10k_gitlab_webhook
rubygem-colorize
rubygem-corineus
rubygem-open4
rubygem-Platform
rubygem-POpen4
rubygem-r10k_gitlab_webhook
rubygem-thor
sonarqube-auth-aad
sonarqube-auth-bitbucket
sonarqube-auth-crowd
sonarqube-auth-github
sonarqube-auth-gitlab
sonarqube-auth-google
sonarqube-auth-saml
sonarqube-csharp
sonarqube-css
sonarqube-dependency-check
sonarqube-findbugs
sonarqube-flexgit push --set-upstream origin fix_mattermost_el8
sonarqube-go
sonarqube-html
sonarqube-jacoco
sonarqube-javascript
sonarqube-java
sonarqube-kotlin
sonarqube-ldap
sonarqube-perl
sonarqube-php
sonarqube-pmd
sonarqube-python
sonarqube-qualinsight-badges
sonarqube-ruby
sonarqube-scala
sonarqube-scm-git
sonarqube-scm-svn
sonarqube-shellcheck
sonarqube
sonarqube-typescript
sonarqube-vbnet
sonarqube-xml
sonarqube-yaml
sops
terraform
terraform-provider-ddcloud
terragrunt
tomcat8
tomcat8-native
tomcat9
tomcat9-native
vault
verdaccio
wildfly)
sonarqube_packages=(sonarqube-auth-aad
sonarqube-auth-bitbucket
sonarqube-auth-crowd
sonarqube-auth-github
sonarqube-auth-gitlab
sonarqube-auth-google
sonarqube-auth-saml
sonarqube-csharp
sonarqube-css
sonarqube-dependency-check
sonarqube-findbugs
sonarqube-flexgit push --set-upstream origin fix_mattermost_el8
sonarqube-go
sonarqube-html
sonarqube-jacoco
sonarqube-javascript
sonarqube-java
sonarqube-kotlin
sonarqube-ldap
sonarqube-perl
sonarqube-php
sonarqube-pmd
sonarqube-python
sonarqube-qualinsight-badges
sonarqube-ruby
sonarqube-scala
sonarqube-scm-git
sonarqube-scm-svn
sonarqube-shellcheck
sonarqube
sonarqube-typescript
sonarqube-vbnet
sonarqube-xml
sonarqube-yaml
)
for i in specs/*.spec; do
  name=$(basename $i .spec)
  if [[ ! " ${not_el7_packages[*]} " == *" ${name} "* ]]; then
    cat <<-EOF >> jobs.yml
    srpm:el7:main:${name}:
      extends: .srpm_el7
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    rpm:el7:main:${name}:
      extends: .rpm_el7
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    copr:el7:main:${name}:
      extends: .copr_el7
      dependencies:
        - srpm:el7:main:${name}
      only:
        refs:
          - master@harbottle/harbottle-main
        changes:
          - specs/${name}.spec
          - sources/${name}/*


EOF

  fi

  if [[ " ${el8_packages[*]} " == *" ${name} "* ]]; then
    cat <<-EOF >> jobs.yml
    srpm:el8:main:${name}:
      extends: .srpm_el8
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    rpm:el8:main:${name}:
      extends: .rpm_el8
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    copr:el8:main:${name}:
      extends: .copr_el8
      dependencies:
        - srpm:el8:main:${name}
      only:
        refs:
          - master@harbottle/harbottle-main
        changes:
          - specs/${name}.spec
          - sources/${name}/*


EOF

  fi

  if [[ " ${sonarqube_packages[*]} " == *" ${name} "* ]]; then
    cat <<-EOF >> jobs.yml
    srpm:el7:sonarqube:${name}:
      extends: .srpm_el7_sonarqube
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    rpm:el7:sonarqube:${name}:
      extends: .rpm_el7_sonarqube
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    copr:el7:sonarqube:${name}:
      extends: .copr_el7_sonarqube
      dependencies:
        - srpm:el7:sonarqube:${name}
      only:
        refs:
          - master@harbottle/harbottle-main
        changes:
          - specs/${name}.spec
          - sources/${name}/*


    srpm:el8:sonarqube:${name}:
      extends: .srpm_el8_sonarqube
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    rpm:el8:sonarqube:${name}:
      extends: .rpm_el8_sonarqube
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    copr:el8:sonarqube:${name}:
      extends: .copr_el8_sonarqube
      dependencies:
        - srpm:el8:sonarqube:${name}
      only:
        refs:
          - master@harbottle/harbottle-main
        changes:
          - specs/${name}.spec
          - sources/${name}/*


EOF

  fi
done
