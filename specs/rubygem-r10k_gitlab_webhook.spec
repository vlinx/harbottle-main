%global gem_name r10k_gitlab_webhook

Name:          rubygem-%{gem_name}
Version:       0.1.3
Release:       5%{?dist}.harbottle
Summary:       Web hook to trigger r10k deploy
Group:         Applications/System
License:       MIT
URL:           https://github.com/spuder/r10k_gitlab_webhook
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
Ruby gem that starts a webrick webservice and listens for requests. When a POST
is recieved, it executes the following command:

  sudo r10k deploy environment -pv debug

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%files
%exclude %{gem_instdir}/.*
%dir %{gem_instdir}
%{gem_instdir}/bin
%{_bindir}/%{gem_name}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Sat Jan 04 2020 - harbottle@room3d3.com - 0.1.3-5
  - Improve dependencies

* Mon Dec 30 2019 - harbottle@room3d3.com - 0.1.3-1
  - Initial packaging
