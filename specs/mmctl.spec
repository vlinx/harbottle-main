Name:          mmctl
Version:       5.21.0
Release:       1%{?dist}.harbottle
Summary:       A remote CLI tool for Mattermost
Group:         Applications/System
License:       Unknown
Url:           https://github.com/mattermost/%{name}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang golangci-lint make

%description
A remote CLI tool for Mattermost: the Open Source, self-hosted
Slack-alternative.

%prep
%setup -q

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
ADVANCED_VET=false BUILD_HASH=harbottle make build

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
#%license LICENSE
%doc *.md docs/*
%{_bindir}/%{name}

%changelog
* Sun Mar 15 2020 - harbottle@room3d3.com - 5.21.0-1
  - Initial package
