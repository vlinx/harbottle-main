%global namespace github.com/hashicorp/%{name}
%global go_version 1.13.6

Name:          terraform
Version:       0.12.24
Release:       1%{?dist}.harbottle
Summary:       Write, Plan, and Create Infrastructure as Code.
Group:         Applications/System
License:       MPL-2.0
URL:           https://terraform.io/
Source0:       https://%{namespace}/archive/v%{version}.tar.gz
%if 0%{?rhel} == 8
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
%else
BuildRequires: golang >= 1.13
%endif
BuildRequires: make which zip

%description
Terraform enables you to safely and predictably create, change, and improve
production infrastructure. It is an open source tool that codifies APIs into
declarative configuration files that can be shared amongst team members, treated
as code, edited, reviewed, and versioned.

%prep
%setup -q
%if 0%{?rhel} == 8
%setup -q -T -D -a 1 -n %{name}-%{version}
%endif

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
%if 0%{?rhel} == 8
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
%else
export PATH=${PATH}:${GOPATH}/bin
%endif
export XC_ARCH=amd64
export XC_OS=linux
mkdir -p src/%{namespace}
shopt -s extglob dotglob
%if 0%{?rhel} == 8
mv !(src|go) src/%{namespace}
%else
mv !(src) src/%{namespace}
%endif
shopt -u extglob dotglob
pushd src/%{namespace}
make tools && make bin
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/%{namespace}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf %{buildroot}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{BUILDING.md,CHANGELOG.md,README.md}
%{_bindir}/%{name}

%changelog
* Thu Mar 19 2020 - harbottle@room3d3.com - 0.12.24-1
  - Bump version

* Thu Mar 05 2020 - harbottle@room3d3.com - 0.12.23-1
  - Bump version

* Thu Mar 05 2020 - harbottle@room3d3.com - 0.12.22-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.12.21-1
  - Bump version

* Wed Jan 22 2020 - harbottle@room3d3.com - 0.12.20-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 0.12.19-2
  - Build for el8
  - Tidy spec

* Wed Jan 08 2020 - harbottle@room3d3.com - 0.12.19-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 0.12.18-1
  - Bump version

* Mon Dec 02 2019 - harbottle@room3d3.com - 0.12.17-1
  - Bump version

* Tue Nov 19 2019 - harbottle@room3d3.com - 0.12.16-1
  - Bump version

* Thu Nov 14 2019 - harbottle@room3d3.com - 0.12.15-1
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 0.12.14-1
  - Bump version

* Thu Oct 31 2019 - harbottle@room3d3.com - 0.12.13-1
  - Bump version

* Fri Oct 18 2019 - harbottle@room3d3.com - 0.12.12-1
  - Bump version

* Thu Oct 17 2019 - harbottle@room3d3.com - 0.12.11-1
  - Bump version

* Mon Oct 07 2019 - harbottle@room3d3.com - 0.12.10-1
  - Bump version

* Tue Sep 17 2019 - harbottle@room3d3.com - 0.12.9-1
  - Bump version

* Wed Sep 04 2019 - harbottle@room3d3.com - 0.12.8-1
  - Bump version

* Thu Aug 22 2019 - harbottle@room3d3.com - 0.12.7-1
  - Bump version

* Wed Jul 31 2019 - harbottle@room3d3.com - 0.12.6-1
  - Bump version

* Thu Jul 18 2019 - harbottle@room3d3.com - 0.12.5-1
  - Bump version

* Thu Jul 11 2019 - harbottle@room3d3.com - 0.12.4-1
  - Bump version

* Mon Jun 24 2019 - harbottle@room3d3.com - 0.12.3-1
  - Bump version

* Wed Jun 12 2019 - harbottle@room3d3.com - 0.12.2-1
  - Bump version

* Wed Jun 05 2019 - harbottle@room3d3.com - 0.12.1-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Mon Mar 11 2019 - harbottle@room3d3.com - 0.11.13-1
  - Bump version

* Fri Mar 08 2019 - harbottle@room3d3.com - 0.11.12-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 0.11.11-3
  - Build from source

* Sat Jan 26 2019 - harbottle@room3d3.com - 0.11.11-2
  - Use remote license file

* Wed Jan 02 2019 - harbottle@room3d3.com - 0.11.11-1
  - Bump version

* Mon Oct 15 2018 - Richard Grainger <grainger@gmail.com> - 0.11.8-1
- Bump version
* Mon Apr 30 2018 - Richard Grainger <grainger@gmail.com> - 0.11.7-1
- Bump version
* Fri Apr 06 2018 - Richard Grainger <grainger@gmail.com> - 0.11.6-1
- Bump version
* Sat Mar 03 2018 - Richard Grainger <grainger@gmail.com> - 0.11.3-1
- Bump version
* Mon Dec 04 2017 - Richard Grainger <grainger@gmail.com> - 0.11.1-1
- Bump version
* Mon Nov 20 2017 - Richard Grainger <grainger@gmail.com> - 0.11.0-1
- Bump version
* Wed Nov 01 2017 - Richard Grainger <grainger@gmail.com> - 0.10.8-1
- Initial spec, based on
  https://github.com/phrawzty/terraform-rpm/blob/master/terraform.spec
