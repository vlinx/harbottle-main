%global homedir %{_datadir}/wildfly
%define release 5.el7.harbottle

Name:             wildfly-oracle
Version:          11.2.0.4
Release:          %{release}
Summary:          Oracle JDBC Drivers for WildFly Application Server
License:          MIT
URL:              https://gitlab.com/harbottle/centos7-wildfly
Source0:          module.xml
BuildArch:        noarch
Requires:         wildfly >= 10.1.0
Requires:         oracle-instantclient11.2-basic >= 11.2.0.4

%description
Links the Oracle JDBC Drivers (ojdbc6.jar) in the Oracle InstantClient to
WildFly Application Server.

#%prep
#%setup -q -n wildfly-%{version}

%install

install -d -m 755 $RPM_BUILD_ROOT%{homedir}
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system/layers
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base/com
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base/com/oracle
install -d -m 755 $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base/com/oracle/main
install -m 644 %{SOURCE0} $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base/com/oracle/main/module.xml
ln -s /usr/lib/oracle/11.2/client64/lib/ojdbc6.jar $RPM_BUILD_ROOT%{homedir}/modules/system/layers/base/com/oracle/main/ojdbc6.jar

%files
%{homedir}/modules/system/layers/base/com/oracle/main/*

%changelog
* Sat Mar 03 2018 grainger@gmail.com
- Update iteration

* Thu Jun 15 2017 grainger@gmail.com
- Improve release naming

* Tue Feb 28 2017 Richard Grainger - 11.2.0.4
- Initial release
