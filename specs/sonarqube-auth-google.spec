%define __jar_repack 0

Name:     sonarqube-auth-google
Version:  1.6.1
Release:  2%{?dist}.harbottle
Summary:  SonarQube Google authentication plugin
Group:    Applications/System
License:  Apache-2.0
URL:      https://github.com/InfoSec812/sonar-auth-google
Source0:  %{url}/releases/download/%{version}/sonar-auth-googleoauth-plugin-%{version}.jar
Autoprov: no

%description
Google authentication plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-auth-google-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-google-plugin-*.jar
rm -f %{_var}/lib/sonarqube/extensions/pluginssonar-auth-googleoauth-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-google-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 1.6.1-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.6.1-1
  - Initial packaging
