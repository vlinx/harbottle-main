%global namespace go.mozilla.org/%{name}/v3

Name:          sops
Version:       3.5.0
Release:       1%{?dist}.harbottle
Summary:       sops is an editor of encrypted files
Group:         Applications/System
License:       MPL-2.0
Url:           https://github.com/mozilla/%{name}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang

%description
sops is an editor of encrypted files that supports YAML, JSON and BINARY formats
and encrypts with AWS KMS, GCP KMS, Azure Key Vault and PGP.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPATH=$PWD
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
make install
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,*.rst}
%{_bindir}/%{name}

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 3.5.0-1
  - Fix source URL
  - Tidy spec file
  - Bump version

* Thu Sep 12 2019 - harbottle@room3d3.com - 3.4.0-1
  - Bump version

* Tue Jun 11 2019 - harbottle@room3d3.com - 3.3.1-1
  - Bump version

* Thu Apr 18 2019 - harbottle@room3d3.com - 3.3.0-1
  - Bump version

* Tue Jan 08 2019 - harbottle@room3d3.com - 3.2.0-1
  - Initial package
