Name:     bower
Version:  1.8.8
Release:  2%{?dist}.harbottle
Summary:  A package manager for the web
Group:    Applications/System
License:  MIT
Url:      https://%{modname}.io
Requires: nodejs-bower = %{version}

%description
Verdaccio is a lightweight private npm proxy registry built in Node.js.

%prep

%build

%files

%changelog
* Sat Jan 04 2020 - harbottle@room3d3.com - 1.8.8-2
  - Initial package
