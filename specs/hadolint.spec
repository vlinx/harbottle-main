%global git_owner hadolint
%global git_repo hadolint
%global git_archive_file v%{version}.tar.gz
%global git_archive_dir %{git_repo}-%{version}

Name:          %{git_repo}
Version:       1.17.5
Release:       1%{?dist}.harbottle
Summary:       A smarter Dockerfile linter
License:       GPL 3.0
Url:           https://github.com/%{git_owner}/%{git_repo}
Source0:       %{url}/archive/%{git_archive_file}
BuildRequires: glibc-static gmp-static stack

%description
A smarter Dockerfile linter that helps you build best practice Docker images.
The linter is parsing the Dockerfile into an AST and performs rules on top of
the AST. It is standing on the shoulders of ShellCheck to lint the Bash code
inside RUN instructions.

%prep
%setup -q -n %{git_archive_dir}

%build
stack build

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
stack install --local-bin-path $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc README.md docs/*
%{_bindir}/%{name}

%changelog
* Thu Jan 30 2020 - harbottle@room3d3.com - 1.17.5-1
  - Bump version

* Mon Jan 06 2020 - harbottle@room3d3.com - 1.17.4-1
  - Bump version

* Thu Nov 21 2019 - harbottle@room3d3.com - 1.17.3-1
  - Bump version

* Fri Sep 06 2019 - harbottle@room3d3.com - 1.17.2-1
  - Bump version

* Thu Jul 04 2019 - harbottle@room3d3.com - 1.17.1-1
  - Bump version

* Mon Apr 01 2019 - harbottle@room3d3.com - 1.16.3-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 1.16.2-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 1.16.1-1
  - Bump version

* Wed Jan 30 2019 - harbottle@room3d3.com - 1.16.0-1
  - Bump version

* Thu Jan 03 2019 - harbottle@room3d3.com - 1.15.0-1
  - Initial package
