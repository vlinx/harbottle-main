%global namespace %{name}.sh/%{name}/v3

Name:          helm
Version:       3.1.2
Release:       1%{?dist}.harbottle
Summary:       The package manager for Kubernetes
Group:         Applications/System
License:       Apache-2.0
Url:           https://github.com/%{name}/%{name}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang make

%description
Helm helps you manage Kubernetes applications — Helm Charts helps you define,
install, and upgrade even the most complex Kubernetes application.

Charts are easy to create, version, share, and publish — so start using Helm and
stop the copy-and-paste.

The latest version of Helm is maintained by the CNCF - in collaboration with
Microsoft, Google, Bitnami and the Helm contributor community.

%prep
%setup -q -n %{name}-%{version}

%build
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export VERSION=%{version}
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
make build
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/bin/helm $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/*.md
%{_bindir}/helm

%changelog
* Thu Mar 12 2020 - harbottle@room3d3.com - 3.1.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 3.1.1-1
  - Bump version

* Thu Feb 13 2020 - harbottle@room3d3.com - 3.1.0-1
  - Bump version

* Wed Jan 29 2020 - harbottle@room3d3.com - 3.0.3-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 3.0.2-1
  - Bump version

* Mon Dec 09 2019 - harbottle@room3d3.com - 3.0.1-2
  - Build from source again

* Fri Dec 06 2019 - harbottle@room3d3.com - 3.0.1-1
  - Download binary, as build is now broken
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 3.0.0-1
  - Bump version

* Tue Nov 12 2019 - harbottle@room3d3.com - 2.16.1-1
  - Bump version

* Wed Nov 06 2019 - harbottle@room3d3.com - 2.16.0-1
  - Bump version

* Tue Oct 29 2019 - harbottle@room3d3.com - 2.15.2-1
  - Bump version

* Wed Oct 23 2019 - harbottle@room3d3.com - 2.15.1-1
  - Bump version

* Fri Oct 18 2019 - harbottle@room3d3.com - 2.15.0-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 2.14.3-1
  - Bump version

* Thu Jul 11 2019 - harbottle@room3d3.com - 2.14.2-1
  - Bump version

* Wed Jun 05 2019 - harbottle@room3d3.com - 2.14.1-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 2.14.0-1
  - Bump version

* Thu Mar 21 2019 - harbottle@room3d3.com - 2.13.1-1
  - Bump version

* Wed Feb 27 2019 - harbottle@room3d3.com - 2.13.0-1
  - Bump version

* Sun Jan 27 2019 - harbottle@room3d3.com - 2.12.3-1
  - Bump version

* Fri Jan 18 2019 - harbottle@room3d3.com - 2.12.2-1
  - Bump version

* Mon Jan 07 2019 - harbottle@room3d3.com - 2.12.1-1
  - Initial package

