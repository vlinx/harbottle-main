Name:      wine-gecko
Version:   2.47
Release:   1.el7.harbottle
Summary:   Gecko library required for Wine
License:   MPLv1.1 or GPLv2+ or LGPLv2+
Group:     Development/Libraries
URL:       http://wiki.winehq.org/Gecko
Source0:   http://dl.winehq.org/wine/wine-gecko/%{version}/wine-mozilla-%{version}-src.tar.bz2
Source1:   https://dl.winehq.org/wine/wine-gecko/%{version}/wine_gecko-%{version}-x86.msi
Source2:   https://dl.winehq.org/wine/wine-gecko/%{version}/wine_gecko-%{version}-x86_64.msi
BuildArch: noarch

%description
Windows Gecko library required for Wine.

%package -n wine32-gecko
Summary:   Gecko library for 32bit wine
Requires:  wine(x86-32)

%description -n wine32-gecko
Windows Gecko library required for Wine.

%package -n wine64-gecko
Summary:   Gecko library for 64bit wine
Requires:  wine(x86-64)

%description -n wine64-gecko
Windows Gecko library required for Wine.

%prep
%setup -q -c -n wine-mozilla-%{version}

%build

%install
mkdir -p %{buildroot}%{_datadir}/wine/gecko
install -p -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/wine/gecko/wine_gecko-%{version}-x86.msi
install -p -m 0644 %{SOURCE2} %{buildroot}%{_datadir}/wine/gecko/wine_gecko-%{version}-x86_64.msi

%files -n wine32-gecko
%license wine-mozilla-%{version}/LICENSE
%doc wine-mozilla-%{version}/LEGAL
%doc wine-mozilla-%{version}/README.txt
%{_datadir}/wine/gecko/wine_gecko-%{version}-x86.msi

%files -n wine64-gecko
%license wine-mozilla-%{version}/LICENSE
%doc wine-mozilla-%{version}/LEGAL
%doc wine-mozilla-%{version}/README.txt
%{_datadir}/wine/gecko/wine_gecko-%{version}-x86_64.msi

%changelog
* Wed Apr 24 2019 harbottle <harbottle@room3d3.com> - 2.47-1
- Initial package
