Name:          corineus
Version:       0.1.2
Release:       2%{?dist}.harbottle
Summary:       Authenticated updates of DNS records on a MS Windows DNS server
Group:         Applications/System
License:       MIT
URL:           https://gitlab.com/harbottle/corineus
Requires:      rubygem(corineus) = %{version}
BuildArch:     noarch

%description
Corineus is a wrapper for the kinit and nsupdate commands to allow easy
authenticated updates of DNS records on a Microsoft Windows DNS server from
Linux.

%prep

%build

%files

%changelog
* Sat Jan 04 2020 - harbottle@room3d3.com - 0.1.2-2
  - Improve dependencies

* Sat Dec 21 2019 - harbottle@room3d3.com - 0.1.2-1
  - Initial packaging
