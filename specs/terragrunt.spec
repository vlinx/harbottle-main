Name:    terragrunt
Version: 0.23.8
Release: 1%{?dist}.harbottle
Summary: Terragrunt is a thin wrapper for Terraform
Group:   Applications/System
License: MIT
URL:     https://terragrunt.gruntwork.io/
Source0: https://github.com/gruntwork-io/%{name}/archive/v%{version}.tar.gz
Source1: https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64

%description
Terragrunt is a thin wrapper for Terraform that provides extra tools for keeping
your Terraform configurations DRY, working with multiple Terraform modules, and
managing remote state.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE.txt
%doc README.md
%{_bindir}/%{name}

%changelog
* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.8-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.7-1
  - Bump version

* Mon Apr 06 2020 - harbottle@room3d3.com - 0.23.6-1
  - Bump version

* Sat Apr 04 2020 - harbottle@room3d3.com - 0.23.5-1
  - Bump version

* Sun Mar 29 2020 - harbottle@room3d3.com - 0.23.4-1
  - Bump version

* Sat Mar 28 2020 - harbottle@room3d3.com - 0.23.3-1
  - Bump version

* Tue Mar 10 2020 - harbottle@room3d3.com - 0.23.2-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Tue Mar 03 2020 - harbottle@room3d3.com - 0.23.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.22.5-1
  - Bump version

* Wed Feb 12 2020 - harbottle@room3d3.com - 0.21.13-1
  - Bump version

* Mon Feb 10 2020 - harbottle@room3d3.com - 0.21.12-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 0.21.11-1
  - Initial package
