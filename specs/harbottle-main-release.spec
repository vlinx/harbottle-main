Name:           harbottle-main-release
Version:        8
Release:        3%{?dist}
Summary:        Harbottle Main repository configuration
Group:          System Environment/Base
License:        GPLv2
URL:            https://gitlab.com/harbottle/harbottle-main-release
Source0:        https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source1:        harbottle-main-7.repo
Source2:        harbottle-main-8.repo
BuildArch:      noarch
%if 0%{?rhel} == 7
Requires:       redhat-release >=  7
%else
Requires:       redhat-release >=  8
%endif

%description
This package contains the Harbottle Main repository
GPG key as well as configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} ./GPL

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
%if 0%{?rhel} == 7
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/harbottle-main.repo
%else
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/harbottle-main.repo
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 8-3
  - Fix source RPM for el8

* Sun Oct 20 2019 - harbottle@room3d3.com - 8-2
  - Fix dependencies

* Sun Oct 20 2019 - harbottle@room3d3.com - 8-1
  - Update for el8

* Sat Jan 20 2018 grainger@gmail.com
- Fix source download for harbottle-main
* Tue Sep 26 2017 grainger@gmail.com
- Remove GPG key
- Add mirror
* Fri Jun 16 2017 grainger@gmail.com
- Update URL and comments
* Thu Jun 15 2017 grainger@gmail.com
- Initial packaging
