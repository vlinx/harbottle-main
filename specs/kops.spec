%global namespace k8s.io/%{name}

Name:          kops
Version:       1.16.0
Release:       1%{?dist}.harbottle
Summary:       Kubernetes Operations
Group:         Applications/System
License:       Apache-2.0
Url:           https://github.com/kubernetes/%{name}
Source0:       %{url}/archive/%{version}.tar.gz
BuildRequires: golang

%description
kops - Kubernetes Operations

kops helps you create, destroy, upgrade and maintain production-grade, highly
available, Kubernetes clusters from the command line. AWS (Amazon Web Services)
is currently officially supported, with GCE in beta support , and VMware vSphere
in alpha, and other platforms planned.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOPROXY=https://proxy.golang.org
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/*.md
%{_bindir}/%{name}

%changelog
* Sat Feb 29 2020 - harbottle@room3d3.com - 1.16.0-1
  - Bump version

* Fri Feb 07 2020 - harbottle@room3d3.com - 1.15.2-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 1.15.1-1
  - Bump version

* Wed Dec 11 2019 - harbottle@room3d3.com - 1.15.0-1
  - Fix build
  - Tidy spec file
  - Bump version

* Fri Nov 08 2019 - harbottle@room3d3.com - 1.14.1-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 1.14.0-1
  - Bump version

* Wed Sep 25 2019 - harbottle@room3d3.com - 1.13.2-1
  - Bump version

* Wed Sep 25 2019 - harbottle@room3d3.com - 1.13.1-1
  - Bump version

* Fri Aug 02 2019 - harbottle@room3d3.com - 1.12.3-1
  - Bump version

* Fri Jun 21 2019 - harbottle@room3d3.com - 1.12.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.12.1-1
  - Bump version

* Fri Mar 01 2019 - harbottle@room3d3.com - 1.11.1-1
  - Bump version

* Thu Jan 10 2019 - harbottle@room3d3.com - 1.11.0-1
  - Initial package
