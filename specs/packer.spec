%global namespace github.com/hashicorp/%{name}
%global go_version 1.13.8

Name:          packer
Version:       1.5.5
Release:       1%{?dist}.harbottle
Summary:       Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
Group:         Applications/System
License:       MPL-2.0
URL:           https://packer.io/
Source0:       https://%{namespace}/archive/v%{version}.tar.gz
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires: make which git

%description
Packer is a tool for creating identical machine images for multiple platforms
from a single source configuration.
Out of the box Packer comes with support to build images for Amazon EC2,
DigitalOcean, Google Compute Engine, QEMU, VirtualBox, VMware, and more.
Support for more platforms is on the way, and anyone can add new platforms via
plugins.

%prep
%setup -q -n %{name}-%{version}
%setup -q -T -D -a 1 -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
export VERSION=%{version}
export XC_ARCH=amd64
export XC_OS=linux
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
git init && git config user.name %{name} && git config user.email %{name}@hashicorp.com
git add . && git commit -m %{name} && git tag v%{version}
make releasebin
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/%{namespace}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}
ln -s %{_bindir}/%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}.io

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{CHANGELOG.md,CODEOWNERS,README.md}
%{_bindir}/%{name}
%{_bindir}/%{name}.io

%changelog
* Wed Mar 25 2020 - harbottle@room3d3.com - 1.5.5-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.5.4-1
  - Bump version
  - Fix build

* Thu Feb 13 2020 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.5.0-2
  - Build for el8

* Thu Dec 19 2019 - harbottle@room3d3.com - 1.5.0-1
  - Fix build
  - Tidy spec file
  - Bump version

* Mon Nov 04 2019 - harbottle@room3d3.com - 1.4.5-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 1.4.4-1
  - Bump version

* Thu Aug 15 2019 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Wed Jun 26 2019 - harbottle@room3d3.com - 1.4.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.4.1-1
  - Bump version

* Fri Apr 12 2019 - harbottle@room3d3.com - 1.4.0-1
  - Bump version

* Thu Mar 07 2019 - harbottle@room3d3.com - 1.3.5-2
  - Fix BuildRequires

* Mon Mar 04 2019 - harbottle@room3d3.com - 1.3.5-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.3.4-2
  - Build from source

* Wed Jan 30 2019 - harbottle@room3d3.com - 1.3.4-1
  - Bump version

* Mon Jan 07 2019 - harbottle@room3d3.com - 1.3.3-1
  - Add docs, tidy-up, add packer.io symlink
   (https://github.com/hashicorp/packer/issues/1117)

* Mon Jan 07 2019 - vlinx@yahoo.com - 1.3.3-1
  - Initial spec, based on
    https://github.com/phrawzty/packer-rpm/blob/master/packer.spec
