Name:    golangci-lint
Version: 1.24.0
Release: 1%{?dist}.harbottle
Summary: Linters Runner for Go
Group:   Applications/System
License: Apache-2.0
Url:     https://github.com/golangci/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}-%{version}-linux-amd64.tar.gz

%description
GolangCI-Lint is a linters aggregator. It's fast: on average 5 times faster than
gometalinter. It's easy to integrate and use, has nice output and has a minimum
number of false positives. It supports go modules.

GolangCI-Lint has integrations with VS Code, GNU Emacs, Sublime Text.

%prep
%setup -qn %{name}-%{version}-linux-amd64

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}

%changelog
* Tue Mar 17 2020 - harbottle@room3d3.com - 1.24.0-1
  - Initial package
