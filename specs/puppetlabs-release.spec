Name:           puppetlabs-release
Version:        22.0
Release:        3.el7
Summary:        Configuration for yum.puppetlabs.com

Group:          System Environment/Base
License:        ASL 2.0

# This is a Puppet Labs maintained package which is specific to
# our distribution.  Thus the source is only available from
# within this srpm.
URL:            http://yum.puppetlabs.com
Source0:        RPM-GPG-KEY-puppet.asc
Source1:        http://yum.puppetlabs.com/RPM-GPG-KEY-puppetlabs
Source2:        RPM-GPG-KEY-nightly-puppetlabs
Source3:        puppetlabs.repo

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires:       redhat-release >=  7
Provides:       puppetlabs-release-devel >= 7-2
Obsoletes:      puppetlabs-release-devel <= 7-1

%description
This package contains the yum.puppetlabs.com repository
GPG key as well as configuration for a yum client.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .
install -pm 644 %{SOURCE3} .


%build

%install
rm -rf $RPM_BUILD_ROOT

#GPG Key
install -Dpm 644 %{SOURCE0} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-puppet
install -Dpm 644 %{SOURCE1} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-puppetlabs
install -Dpm 644 %{SOURCE2} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-nightly-puppetlabs

# yum
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE3}  \
    $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/puppetlabs.repo
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-puppet
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-puppetlabs
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-nightly-puppetlabs


%changelog
* Wed Aug 23 2017 <grainger@gmail.com> -  22.0-3.el7
- Fix release number

* Thu Sep 08 2016 Puppet Labs Release <info@puppetlabs.com> -  22.0-2
- Build for 22.0

* Fri Aug 19 2016 Rob Braden <bradejr@puppet.com>
- Add new puppet, inc key to puppetlabs-release package

* Fri Aug 22 2014 Matthaus Owens <matthaus@puppetlabs.com>
- Add nightly key to puppetlabs-release package

* Thu Jun 28 2012 Matthaus Litteken <matthaus@puppetlabs.com> -  22.0-2
- Update %files section, obsolete devel package

* Sat Sep 24 2011 Michael Stahnke <stahnma@puppetlabs.com> -  22.0-1
- Initial Package
