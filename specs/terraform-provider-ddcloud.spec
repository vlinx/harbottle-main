%global repo dd-cloud-compute-terraform
%global namespace github.com/DimensionDataResearch/%{repo}

Name:          terraform-provider-ddcloud
Version:       2.3.3
Release:       3%{?dist}.harbottle
Summary:       Terraform provider for Dimension Data cloud compute.
Group:         Applications/System
License:       MIT
URL:           https://%{namespace}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang make git
Requires:      terraform

%description
Terraform provider for Dimension Data cloud compute.

%prep
%setup -q -n %{repo}-%{version}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOOS=linux
export GOARCH=amd64
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
go get github.com/DimensionDataResearch/go-dd-cloud-compute/compute
go get github.com/hashicorp/terraform/helper/schema
go get github.com/hashicorp/terraform/plugin
go get github.com/hashicorp/terraform/terraform
go get golang.org/x/crypto/pkcs12
go get github.com/pkg/errors
echo "package ddcloud" > ddcloud/version-info.go
echo "" >> ddcloud/version-info.go
echo "const ProviderVersion = \"v%{version}\"" >> ddcloud/version-info.go
go build -o bin/%{name}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/%{namespace}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf %{buildroot}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{CHANGES.md,CONTRIBUTING.md,CONTRIBUTORS.md,README.md}
%{_bindir}/%{name}

%changelog
* Sat Jan 18 2020 - harbottle@room3d3.com - 2.3.3-3
  - Fix el8 build

* Sat Jan 18 2020 - harbottle@room3d3.com - 2.3.3-2
  - Build for el8
  - Tidy spec

* Thu Jan 09 2020 - harbottle@room3d3.com - 2.3.3-1
  - Bump version

* Mon Oct 28 2019 - harbottle@room3d3.com - 2.3.2-1
  - Bump version

* Tue Jul 16 2019 - harbottle@room3d3.com - 2.2.2-1
  - Bump version

* Fri Apr 12 2019 - harbottle@room3d3.com - 2.2.1-1
  - Bump version

* Tue Mar 19 2019 - harbottle@room3d3.com - 2.1.2-1
  - Bump version

* Thu Feb 21 2019 - harbottle@room3d3.com - 2.0.1-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.3.8-3
  - Build from source

* Sat Jan 26 2019 - harbottle@room3d3.com - 1.3.8-2
  - Use remote license file

* Mon Jun 11 2018 - Richard Grainger <grainger@gmail.com> - 1.3.8-1
- Bump version

* Tue May 01 2018 - Richard Grainger <grainger@gmail.com> - 1.3.6-1
- Bump version

* Fri Apr 06 2018 - Richard Grainger <grainger@gmail.com> - 1.3.4-1
- Bump version

* Fri Dec 22 2017 - Richard Grainger <grainger@gmail.com> - 1.3.2-1
- Bump version

* Wed Nov 01 2017 - Richard Grainger <grainger@gmail.com> - 1.3.1-1
- Initial spec.

