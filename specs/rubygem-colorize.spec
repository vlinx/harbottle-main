%global gem_name colorize

Name:          rubygem-%{gem_name}
Version:       0.8.1
Release:       3%{?dist}.harbottle
Summary:       Set text color, background color and text effects
Group:         Applications/System
License:       GPL-2.0
URL:           http://github.com/fazibear/colorize
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
Extends String class or add a ColorizedString with methods to set text color,
background color and text effects.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

%check

%files
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/README.md
%license %{gem_instdir}/LICENSE
%doc %{gem_instdir}/CHANGELOG
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
 
%files doc
%doc %{gem_docdir}
%{gem_instdir}/Rakefile
%{gem_instdir}/%{gem_name}.gemspec
%{gem_instdir}/test/

%changelog
* Sat Dec 21 2019 - harbottle@room3d3.com - 0.1.2-3
  - Improve dependencies

* Fri Dec 20 2019 - harbottle@room3d3.com - 0.1.2-2
  - Add group

* Fri Dec 20 2019 - harbottle@room3d3.com - 0.1.2-1
  - Initial packaging
