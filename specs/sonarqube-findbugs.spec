%define __jar_repack 0

Name:     sonarqube-findbugs
Version:  3.11.1
Release:  1%{?dist}.harbottle
Summary:  SonarQube SpotBugs plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/spotbugs/sonar-findbugs
Source0:  %{url}/releases/download/%{version}/sonar-findbugs-plugin-%{version}.jar
Autoprov: no

%description
SpotBugs plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-findbugs-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-findbugs-plugin-%{version}.jar

%changelog
* Fri Jan 03 2020 - harbottle@room3d3.com - 3.11.1-1
  - Initial package
