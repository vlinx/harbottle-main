%define __jar_repack 0

Name:     sonarqube-php
Version:  3.3.0.5166
Release:  2%{?dist}.harbottle
Summary:  SonarQube PHP plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-php-plugin/sonar-php-plugin-%{version}.jar
Autoprov: no

%description
PHP plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-php-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-php-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 3.3.0.5166-2
  - Spec file changes for el8

* Mon Dec 09 2019 - harbottle@room3d3.com - 3.3.0.5166-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.2.0.4868-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 3.2.0.4868-1
  - Initial package
