%global contentdir %{_datadir}/netdata

# This is temporary and should eventually be resolved. This bypasses
# the default rhel __os_install_post which throws a python compile
# error.
%global __os_install_post %{nil}

Summary:          Real-time performance monitoring, done right
Name:             netdata
Version:          1.11.1
Release:          1.el7.harbottle
License:          GPLv3+
Group:            Applications/System
Source0:          https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-v%{version}.tar.gz
Source1:          netdata.sh
URL:              http://my-netdata.io
BuildRequires:    pkgconfig
BuildRequires:    xz
BuildRequires:    zlib-devel
BuildRequires:    libuuid-devel
BuildRequires:    libmnl-devel
BuildRequires:    libnetfilter_acct-devel
BuildRequires:    systemd
Requires:         zlib
Requires:         libuuid
Requires:         libmnl
Requires:         libnetfilter_acct
Requires(pre):    /usr/sbin/groupadd
Requires(pre):    /usr/sbin/useradd
Requires(post):   libcap
Requires(preun):  systemd-units
Requires(postun): systemd-units
Requires(post):   systemd-units

%description
netdata is the fastest way to visualize metrics. It is a resource
efficient, highly optimized system for collecting and visualizing any
type of realtime timeseries data, from CPU usage, disk activity, SQL
queries, API calls, web site visitors, etc.

netdata tries to visualize the truth of now, in its greatest detail,
so that you can get insights of what is happening now and what just
happened, on your systems and applications.

%prep
%setup -q -n %{name}-%{version}_rolling

%build
%configure \
  --with-zlib \
  --with-math \
  --enable-plugin-nfacct \
  --with-user=netdata
%{__make} %{?_smp_mflags}

%install
rm -rf "${RPM_BUILD_ROOT}"
%{__make} %{?_smp_mflags} DESTDIR="${RPM_BUILD_ROOT}" install

find "${RPM_BUILD_ROOT}" -name .keep -delete

install -m 644 -p system/netdata.conf "${RPM_BUILD_ROOT}%{_sysconfdir}/%{name}"
install -m 755 -d "${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d"
install -m 644 -p system/netdata.logrotate "${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/%{name}"
install -m 755 -d "${RPM_BUILD_ROOT}%{_unitdir}"
install -m 644 -p system/netdata.service "${RPM_BUILD_ROOT}%{_unitdir}/netdata.service"
mkdir -p "${RPM_BUILD_ROOT}%{_bindir}"
mv "${RPM_BUILD_ROOT}%{_sysconfdir}/%{name}/edit-config" "${RPM_BUILD_ROOT}%{_bindir}/%{name}-edit-config"
install -d -m 755 "$RPM_BUILD_ROOT%{_sysconfdir}/profile.d"
install -m 644 %{SOURCE1} "${RPM_BUILD_ROOT}%{_sysconfdir}/profile.d/%{name}.sh"

%pre
getent group netdata >/dev/null || groupadd -r netdata
getent group docker >/dev/null || groupadd -r docker
getent passwd netdata >/dev/null || \
  useradd -r -g netdata -G docker -s /sbin/nologin \
    -d %{contentdir} -c "netdata" netdata

%post
%systemd_post netdata.service

%preun
%systemd_postun_with_restart netdata.service

%postun
%systemd_postun_with_restart netdata.service

%clean
rm -rf "${RPM_BUILD_ROOT}"

%files
%doc README.md
%defattr(-,root,root)

%dir %{_sysconfdir}/%{name}

%{_bindir}/%{name}-edit-config

%config(noreplace) %{_sysconfdir}/%{name}/*.conf
%config(noreplace) %{_sysconfdir}/%{name}/charts.d
%config(noreplace) %{_sysconfdir}/%{name}/health.d
%config(noreplace) %{_sysconfdir}/%{name}/node.d
%config(noreplace) %{_sysconfdir}/%{name}/python.d
%config(noreplace) %{_sysconfdir}/%{name}/statsd.d

%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%{_sysconfdir}/profile.d/%{name}.sh

%{_sbindir}/%{name}


%{_libexecdir}/%{name}/charts.d
%{_libexecdir}/%{name}/node.d
%{_libexecdir}/%{name}/plugins.d/alarm-email.sh
%{_libexecdir}/%{name}/plugins.d/alarm-notify.sh
%{_libexecdir}/%{name}/plugins.d/alarm-test.sh
%caps(cap_dac_read_search,cap_sys_ptrace=ep) %attr(0555,root,root) %{_libexecdir}/%{name}/plugins.d/apps.plugin
%{_libexecdir}/%{name}/plugins.d/cgroup-name.sh
%{_libexecdir}/%{name}/plugins.d/cgroup-network
%{_libexecdir}/%{name}/plugins.d/cgroup-network-helper.sh
%{_libexecdir}/%{name}/plugins.d/charts.d.dryrun-helper.sh
%{_libexecdir}/%{name}/plugins.d/charts.d.plugin
%{_libexecdir}/%{name}/plugins.d/fping.plugin
%{_libexecdir}/%{name}/plugins.d/loopsleepms.sh.inc
%{_libexecdir}/%{name}/plugins.d/node.d.plugin
%{_libexecdir}/%{name}/plugins.d/python.d.plugin
%{_libexecdir}/%{name}/plugins.d/tc-qos-helper.sh
%{_libexecdir}/%{name}/python.d

%attr(0700,netdata,netdata) %dir %{_localstatedir}/cache/%{name}
%attr(0700,netdata,netdata) %dir %{_localstatedir}/log/%{name}
%attr(0700,netdata,netdata) %dir %{_localstatedir}/lib/%{name}

%dir %{_datadir}/%{name}

%{_unitdir}/netdata.service

%{_libdir}/%{name}

# Enforce 0644 for files and 0755 for directories
# for the netdata web directory
%defattr(0644,root,netdata,0755)
%{_datadir}/%{name}/web

%changelog
* Mon Dec 31 2018 - harbottle@room3d3.com - 1.11.1-1
  - Bump version
* Wed Apr 18 2018 Richard Grainger <grainger@gmail.com> - 1.10.0-1
- Bump version
* Wed Apr 18 2018 Richard Grainger <grainger@gmail.com> - 1.6.0-2
- Simplified for harbottle-main EL7 repo
* Mon Mar 20 2017 Costa Tsaousis <costa@tsaousis.gr> - 1.6.0-1
- central netdata
- monitoring ephemeral nodes
- monitoring ephemeral containers and VM guests
- apps.plugin ported for FreeBSD
- web_log plugin
- JSON backends
- IPMI monitoring
- several new and improved plugins
- several new and improved alarms and notifications
- dozens more improvements and bug fixes
* Sun Jan 22 2017 Costa Tsaousis <costa@tsaousis.gr> - 1.5.0-1
- FreeBSD, MacOS, FreeNAS
- Backends support
- dozens of new and improved plugins
- dozens of new and improved alarms and notification methods
* Tue Oct 4 2016 Costa Tsaousis <costa@tsaousis.gr> - 1.4.0-1
- the fastest netdata ever (with a better look too)!
- improved IoT and containers support!
- alarms improved in almost every way!
- Several more improvements, new features and bugfixes.
* Sun Aug 28 2016 Costa Tsaousis <costa@tsaousis.gr> - 1.3.0-1
- netdata now has health monitoring
- netdata now generates badges
- netdata now has python plugins
- Several more improvements, new features and bugfixes.
* Tue Jul 26 2016 Jason Barnett <J@sonBarnett.com> - 1.2.0-2
- Added support for EL6
- Corrected several Requires statements
- Changed default to build without nfacct
- Removed --docdir from configure
* Mon May 16 2016 Costa Tsaousis <costa@tsaousis.gr> - 1.2.0-1
- netdata is now 30% faster.
- netdata now has a registry (my-netdata menu on the dashboard).
- netdata now monitors Linux containers.
- Several more improvements, new features and bugfixes.
* Wed Apr 20 2016 Costa Tsaousis <costa@tsaousis.gr> - 1.1.0-1
- Several new features (IPv6, SYNPROXY, Users, Users Groups).
- A lot of bug fixes and optimizations.
* Tue Mar 22 2016 Costa Tsaousis <costa@tsaousis.gr> - 1.0.0-1
- First public release.
* Sun Nov 15 2015 Alon Bar-Lev <alonbl@redhat.com> - 0.0.0-1
- Initial add.
