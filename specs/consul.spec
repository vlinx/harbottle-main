%global namespace github.com/hashicorp/%{name}
%global confdir %{_sysconfdir}/%{name}.d
%global datadir %{_var}/lib/%{name}/
%global go_version 1.13.8

Name:             consul
Version:          1.7.2
Release:          1%{?dist}.harbottle
Summary:          Consul is a tool for service discovery and configuration
Group:            Applications/System
License:          MPL-2.0
URL:              https://consul.io/
Source0:          https://%{namespace}/archive/v%{version}.tar.gz
Source1:          %{name}.hcl
Source2:          %{name}.service
Source3:          https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires:    git make systemd-units which
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Consul is a tool for service discovery and configuration. Consul is distributed,
highly available, and extremely scalable.

Consul provides several key features:

- Service Discovery - Consul makes it simple for services to register themselves
  and to discover other services via a DNS or HTTP interface. External services
  such as SaaS providers can be registered as well.

- Health Checking - Health Checking enables Consul to quickly alert operators
  about any issues in a cluster. The integration with service discovery prevents
  routing traffic to unhealthy hosts and enables service level circuit breakers.

- Key/Value Storage - A flexible key/value store enables storing dynamic
  configuration, feature flagging, coordination, leader election and more. The
  simple HTTP API makes it easy to use anywhere.

- Multi-Datacenter - Consul is built to be datacenter aware, and can support any
  number of regions without complex configuration.

- Service Segmentation - Consul Connect enables secure service-to-service
  communication with automatic TLS encryption and identity-based authorization.

%prep
%setup -q -n %{name}-%{version}
%setup -q -T -D -a 3 -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
git init && git config user.name %{name} && git config user.email %{name}@hashicorp.com
git add . && git commit -m %{name} && git tag v%{version}
make tools && make linux
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{confdir}
install -d -m 755 $RPM_BUILD_ROOT%{datadir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

install -m 0755 src/%{namespace}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}
install -m 0640 %{SOURCE1} $RPM_BUILD_ROOT%{confdir}
install -m 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{confdir} -s /sbin/nologin -c "%{name} service user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license src/%{namespace}/{LICENSE,NOTICE.md}
%doc src/%{namespace}/{CHANGELOG.md,INTERNALS.md,README.md}
%{_bindir}/%{name}
%attr(-,%{name},%{name}) %config(noreplace) %{confdir}
%attr(-,%{name},%{name}) %{datadir}
%{_unitdir}/%{name}.service

%changelog
* Mon Mar 16 2020 - harbottle@room3d3.com - 1.7.2-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 1.7.1-1
  - Bump version
  - Fix build

* Tue Feb 11 2020 - harbottle@room3d3.com - 1.7.0-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 1.6.3-1
  - Bump version

* Fri Dec 20 2019 - harbottle@room3d3.com - 1.6.2-2
  - Build for el8
  - Tidy spec file

* Thu Nov 14 2019 - harbottle@room3d3.com - 1.6.2-1
  - Bump version

* Thu Sep 12 2019 - harbottle@room3d3.com - 1.6.1-1
  - Bump version

* Mon Aug 26 2019 - harbottle@room3d3.com - 1.6.0-1
  - Bump version

* Fri Jul 26 2019 - harbottle@room3d3.com - 1.5.3-1
  - Bump version

* Thu Jun 27 2019 - harbottle@room3d3.com - 1.5.2-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 1.5.1-1
  - Bump version

* Thu Mar 21 2019 - harbottle@room3d3.com - 1.4.4-1
  - Bump version

* Tue Mar 05 2019 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.4.2-2
  - Mark config files with noreplace

* Sat Feb 02 2019 - harbottle@room3d3.com - 1.4.2-1
  - Initial package
